package Files;

//Napisz program kopiujący pliki, których nazwy zostały podane jako parametry wywołania.
//        W metodzie main pobierz od użytkownika dwie ścieżki do plików (*łatwe, przypisz na sztywno ścieżkę do plików),
// a następnie przekaż te ścieżki do metody:
//        kopiuj (String sourcePath, String targetPath)
//        Metoda ta tworzy nowe obiekty File{Input,Outptu}Stream, a następnie kopiuje bit po bicie dane z jednego
// strumienia do drugiego. Jeżeli pliki nie istnieją, wyświetl taką informację dla użytkownika.

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Wpisz sciezke pliku: ");
//        String sourcePath = sc.nextLine();
//        System.out.println("Wpisz sciezke docelowa: ");
//        String targetPath = sc.nextLine();
        copyFile("/home/beks/IdeaProjects/Zad48/src/com/beksand/Folder/file1.txt",
                "/home/beks/IdeaProjects/Zad48/src/com/beksand/Folder/file2.txt");
    }
    private static void copyFile(String sourcePath, String targetPath) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(new File(targetPath));
        Scanner sc = new Scanner(new File(sourcePath));
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            line += "\n";
            outputStream.write(line.getBytes());
        }
    }
}

